var PSTO = PSTO || {};

(function( $ ) {
	var Carousel = PSTO.Carousel = function Carousel(el) {
		// Elements

		var $el       = $(el);
		var $cont     = $('.car-item-cont', $el);
		var $item     = $('.car-item', $el);
		var $content  = $('.car-item-content', $el);
		var $mask     = $('.car-mask', $el);
		var $next_btn = $('.car-nav-next', $el);
		var $prev_btn = $('.car-nav-prev', $el);

		// Variables

		var NUM_ITEMS  = $item.length;
		var MIN_MARGIN = 10;
		var SPEED      = 750;

		var item_width    = $content.width();
		var position      = 0;
		var increment     = Math.floor($mask.width() / item_width);
		var cont_position = $cont.position().left;
		var cont_width;

		// Public Functions

		this.init = function() {
			// Event Handlers
			$next_btn.on("click", _next);
			$prev_btn.on("click", _prev);
			$(window).on("resize", _resize);

			// Trigger inital sizing
			_resize();

			// Make content visible after sizing is complete
			$content.css('display', 'block');
		}

		// Private Functions

		var _set_increment = function() {
			// Determine how many items can fit within the mask
			var visible_content_width = item_width * increment;
			var visible_padding_width = MIN_MARGIN * (increment - 1);
			var available_space = visible_width - (visible_content_width + visible_padding_width);

			// Adjust the item count if needed
			if (available_space < MIN_MARGIN) {
				increment--;
			}else if (available_space > item_width + (MIN_MARGIN * 2)) {
				increment++;
			}

			// Cap increment if it exceeds limits
			if (increment < 1) {
				increment = 1;
			}else if (increment > NUM_ITEMS) {
				increment = NUM_ITEMS;
			}

			_disable_arrows();

		}

		var _disable_arrows = function() {
			if (position === 0) {
				$prev_btn.addClass('disabled');
			}else if ($prev_btn.hasClass('disabled')) {
				$prev_btn.removeClass('disabled');
			}

			if (position === NUM_ITEMS - increment) {
				$next_btn.addClass('disabled');
			}else if ($next_btn.hasClass('disabled')) {
				$next_btn.removeClass('disabled');
			}
		}

		var _next = function(e) {
			if ($next_btn.hasClass('disabled')) {
				return;
			}

			// Find the movement distance
			position += increment;
			var distance = increment * $item.width();
			var target_position = Math.round(cont_position - distance);
			if(target_position < -cont_width + visible_width) {
				target_position = -cont_width + distance;
				position = NUM_ITEMS - increment;
			}
			// Update container position
			$cont.animate({
				left: target_position
			}, SPEED);
			cont_position = target_position;

			_disable_arrows();
		}

		var _prev = function(e) {
			if ($prev_btn.hasClass('disabled')) {
				return;
			}

			// Find the movement distance
			position -= increment;
			var distance = increment * $item.width();
			var target_position = Math.round(cont_position + distance);
			if(target_position > 0) {
				target_position = 0;
				position = 0;
			}
			// Update container position
			$cont.animate({
				left: target_position
			}, SPEED);
			cont_position = target_position;

			_disable_arrows();
		}

		var _resize = function(e) {
			visible_width = $mask.width();

			_set_increment();

			// Set the width of the items
			var width_percent = 100/increment;
			$item.css('width', width_percent + '%');

			// Set the width of the container
			var width = (visible_width / increment) * NUM_ITEMS;
			width_percent = 100/NUM_ITEMS;
			$cont.css('width', width);
			$item.css('width', width_percent + '%');
			
			cont_width = width;

			// Reposition the container to avoid item overflow
			if(position > NUM_ITEMS - increment) {
				position = NUM_ITEMS - increment;
			}
			if(position < 0) {
				position = 0;
			}
			var target_position = position * $item.width();
			$cont.css('left', -target_position + 'px');
		}
	}

})( jQuery );
